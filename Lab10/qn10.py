#Create a class called “Calculator” with methods for addition, subtraction, multiplication,
#and division. Handle exceptions like division by zero gracefully and raise custom
#exceptions when needed.

class Calculator:
    def __init__(self,a,b):
        self.a=a
        self.b=b
        
    def addition(self):
        return self.a+self.b
    
    def subtraction(self):
        return self.a-self.b
    
    def multiplication(self):
        return self.a*self.b
    
    def division(self):
        try:
            return self.a/self.b
        except ZeroDivisionError:
            print("Division by zero not possible")
            
obj=Calculator(10,0)
print(obj.addition())
print(obj.subtraction())
print(obj.multiplication())
print(obj.division())



