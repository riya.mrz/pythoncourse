# Create classes for “Author” and “Book.” The “Book” class should have an “Author” object as an attribute, demonstrating composition

class Author:
    def __init__(self, name, age, place):
        self.name = name
        self.age = age
        self.place = place
        
class Book:
    def __init__(self, name, price, author):
        self.name = name
        self.price = price
        self.author = author
        
    def display(self):
        print("Book Name:", self.name)
        print("Price:", self.price)
        print("Author Name:", self.author.name)
        print("Author Age:", self.author.age)
        print("Author Place:", self.author.place)
        
a = Author("John", 45, "New York")
b = Book("Python", 500, a)
b.display()