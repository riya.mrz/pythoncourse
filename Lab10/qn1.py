#wap to create a class by name students and initialize attributes like name, age and grade while creating an object

class Students:
    def __init__(self,name,age,grade):
        self.name=name
        self.age=age
        self.grade=grade
    
    def info(self):
        list=[self.name,self.age,self.grade]
        print(list)

std=Students("Raju",20,"A")
std1=Students("Riya",21,"A")
std.info()
std1.info()