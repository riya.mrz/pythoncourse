# Create a base class called “Vehicle” with a method called “drive.” Implement two
# subclasses, “Car” and “Bicycle,” that inherit from “Vehicle” and override the “drive”
# method with their own implementations.

class Vehicle:
    def drive(self):
        pass
    
class Car(Vehicle):
    def drive(self):
        print("Car is being driven")
        
class Bicycle(Vehicle):
    def drive(self):
        print("Bicycle is being driven")
        
obj=Car()
obj.drive()
obj=Bicycle()
obj.drive()
