#Create classes for “Department” and “Employee.” Use aggregation to represent that an
#Employee belongs to a Department. Implement methods to add employees to a
#department and calculate the average salary of employees in a department

class Department:
    def __init__(self, name):
        self.name = name
        self.employees = []
        
    def add_employee(self, employee):
        self.employees.append(employee)
        
    def average_salary(self):
        total = 0
        for employee in self.employees:
            total += employee.salary
        return total/len(self.employees)
    
class Employee:
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        
d = Department("HR")
e1 = Employee("John", 100)
e2 = Employee("Mike", 100)
d.add_employee(e1)
d.add_employee(e2)
print(d.average_salary())

