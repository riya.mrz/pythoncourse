#wap to create three classes person, employee and students use multiple inheritances to create a class personalinfo that inherits from both employee and students add methods and attributes specific to each class

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
class Employee:
    def __init__(self, emp_id, salary):
        self.emp_id = emp_id
        self.salary = salary
        
class Student:
    def __init__(self, roll_no, marks):
        self.roll_no = roll_no
        self.marks = marks
        
class PersonalInfo(Person, Employee, Student):
    def __init__(self, name, age, emp_id, salary, roll_no, marks):
        Person.__init__(self, name, age)
        Employee.__init__(self, emp_id, salary)
        Student.__init__(self, roll_no, marks)
        
    def display(self):
        print("Name:", self.name)
        print("Age:", self.age)
        print("Employee ID:", self.emp_id)
        print("Salary:", self.salary)
        print("Roll No:", self.roll_no)
        print("Marks:", self.marks)
        
p = PersonalInfo("John", 25, 101, 25000, 1, 90)
p.display()
