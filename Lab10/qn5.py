#wap to implement a stack data structure using class and objects with push,pop and traversal method

class stack:
    def __init__(self,element):
        self.element=[]
        
    def push(self,element):
        self.element.append(element)
        
    def pop(self):
        self.element.pop()  
        
    def traversal(self):
        for i in self.element:
            print(i)

obj=stack(0)
obj.push(1)
obj.push(2)
obj.push(3)
obj.push(4)
obj.push(5)
obj.traversal()
obj.pop()
obj.traversal()
obj.pop()
obj.traversal()

