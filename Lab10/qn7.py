#create a base class called Animal and two sub classes dog and cat add methods and attributes specific to each subclass

class Animal:
    def __init__(self,name,age):
        self.name=name
        self.age=age
        
class Dog(Animal):
    def __init__(self,name,age,breed):
        Animal.__init__(self,name,age)   #super() is used to call the __init__ method of the parent class
        self.breed=breed
        
    def bark(self):
        print("Bow Bow")
        
class Cat(Animal):
    def __init__(self,name,age,color):
        Animal.__init__(self,name,age)
        self.color=color
        
    def meow(self):
        print("Meow Meow")
        
obj=Dog("Tommy",5,"Labrador")
print(obj.name)
print(obj.age)
print(obj.breed)
obj.bark()

obj=Cat("Kitty",3,"White")
print(obj.name)
print(obj.age)
print(obj.color)
obj.meow()


