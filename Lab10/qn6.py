#wap to create a class Square and define two methods that return the square area and perimeter

class Square:
    def __init__(self,l):
        self.l=l
    
    def area(self):
        return self.l*self.l
    
    def perimeter(self):
        return 4*self.l
    
obj=Square(5)
print(obj.area())
print(obj.perimeter())

