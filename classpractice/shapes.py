class shapes:
    def __init__ (self,length,breadth,radius):
        self.length=length
        self.breadth=breadth
        self.radius=radius
    
    def rec(self):
        area=(self.length*self.breadth)
        return area
    
    def squ(self):
        area=self.length**2
        return area
    
    def cir(self):
        area=(3.14*self.radius**2)
        return area

print("Area of Diff shapes:")
obj=shapes(2,3,4)
print("Rectangle area:",obj.rec())
print("Square area:",obj.squ())
print("Circle area",obj.cir())

