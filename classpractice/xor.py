# a= 4^6
# print(a)

# Swapping variables
x = 5
y = 10
print(x,y)

x, y = y, x

print("x:", x)  # Output: 10
print("y:", y)  # Output: 5
