class dog:
    def __init__ (self,name,age):
        self.name=name
        self.age=age
    
    def dogname(self):
        print("This cute thing's name is ",self.name)
        print("Welcome, ",self.name)
        print()
    
    def info(self):
        print("Name:",self.name)
        print("Age(in month):",self.age)
        print()

obj=dog("pochi",2)
obj.dogname()
obj.info()

obj1=dog("mochi",3)
obj1.dogname()
obj1.info()