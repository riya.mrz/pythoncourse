# using dict() constructor

dict2 = dict({"hello": 'applephone', 2: 'ball'})
print(dict2)

print(len(dict2))
print(dict2.get("hello"))

print(dict2[2])

print("keys:values")
for i in dict2:
    print(i,":",dict2[i])