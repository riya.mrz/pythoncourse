#Write a program to create a list of numbers in the range 1 to 10. Then delete all the even numbers from the list and print the final list

array=[]
for i in range(1,11):
    array.append(i)
print("The original list is: ",array)
for i in array:
    if i%2==0:
        array.remove(i)
print("The final list is: ",array)
