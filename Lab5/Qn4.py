#Write a program to generate in the Fibonacci series and store it in a list. Then find the sum of all values

fibo=[0,1]
n=int(input("Enter the number of terms in the Fibonacci series: "))
for i in range(2,n):
    fibo.append(fibo[i-1]+fibo[i-2])
print("The Fibonacci series is: ",fibo)
print("The sum of all the values in the Fibonacci series is: ",sum(fibo))




