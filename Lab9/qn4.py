#create a loan program using python setter and getter method

class loan:
    def __init__(self):
        self._principal=0
        self._rate=0
        self._time=0

    def set_principal(self,principal):
        self._principal=principal
    
    def set_rate(self,rate):
        self._rate=rate
    
    def set_time(self,time):
        self._time=time
    
    def get_principal(self):
        return self._principal
    
    def get_rate(self):
        return self._rate
    
    def get_time(self):
        return self._time
    
    def calculate_interest(self):
        return (self._principal*self._rate*self._time)/100

l=loan()
l.set_principal(1000)
l.set_rate(2)
l.set_time(2)
print("Principal:",l.get_principal())
print("Rate:",l.get_rate())
print("Time:",l.get_time())
print("Interest:",l.calculate_interest())
