#My Mini Calculator

from colorama import Fore, Back, Style


class simple_operations:    #class for simple operations (addition, subtraction, multiplication, division)
    def add(self, a, b):
        result=a + b
        print(Fore.LIGHTGREEN_EX +"The sum of the two numbers is: ", result)
        print("")

    def subtract(self, a, b):
        result= a - b
        print(Fore.LIGHTGREEN_EX + "The difference of the two numbers is: ", result)
        print("")

    def multiply(self, a, b):
        result= a * b
        print(Fore.LIGHTGREEN_EX + "The product of the two numbers is: ", result)
        print("")

    def divide(self, a, b):
        try:
            result = a / b
            print(Fore.LIGHTGREEN_EX + "The quotient of the two numbers is: ", result)
        except ZeroDivisionError: #exception handling for division by zero
            print(Fore.LIGHTGREEN_EX + "Error: Division by zero is not allowed.")
        print("")

class advanced_operations: #class for advanced operations (square root, cube root, factorial, power, floor value)
    def square_root(self, a):
        result= a ** 0.5
        print(Fore.LIGHTGREEN_EX + "The square root of the number is: ", result)
        print("")

    def power(self, a, b):
        result= a ** b
        print(Fore.LIGHTGREEN_EX + "The result of the number raised to the power is: ", result)
        print("")
    
    def factorial(self, a):
        result= 1
        for i in range(0, a+1):
            result= result * i
        print(Fore.LIGHTGREEN_EX + "The factorial of the number is: ", result)
        print("")

    def cube_root(self, a):
        result= a ** (1/3)
        print(Fore.LIGHTGREEN_EX + "The cube root of the number is: ", result)
        print("")

    def floor(self, a,b):
        result= a // b
        print(Fore.LIGHTGREEN_EX + "The floor value of the number is: ", result)
        print("")

import math #importing math module for trigonometric operations
class trigonometric_operations: #class for trigonometric operations (sine, cosine, tangent, hyperbolic sine, hyperbolic cosine, hyperbolic tangent)
    def sin(self, a):
        result= math.sin(a)
        print(Fore.LIGHTGREEN_EX + "The sine of the number is: ", result)
        print("")

    def cos(self, a):
        result= math.cos(a)
        print(Fore.LIGHTGREEN_EX + "The cosine of the number is: ", result)
        print("")

    def tan(self, a):
        result= math.tan(a)
        print(Fore.LIGHTGREEN_EX + "The tangent of the number is: ", result)
        print("")

    def sinh(self, a):
        result= math.sinh(a)
        print(Fore.LIGHTGREEN_EX + "The hyperbolic sine of the number is: ", result)
        print("")

    def cosh(self, a):
        result= math.cosh(a)
        print(Fore.LIGHTGREEN_EX + "The hyperbolic cosine of the number is: ", result)
        print("")

    def tanh(self, a):
        result= math.tanh(a)
        print(Fore.LIGHTGREEN_EX + "The hyperbolic tangent of the number is: ", result)
        print("")


print("")
print("")
print(Fore.LIGHTGREEN_EX + Style.BRIGHT +".....Mini Calculator.....")
print(Style.RESET_ALL) #resetting the color to default)
print("")
while True:
    print(Fore.CYAN + "What kind of operation would you like to perform?")
    print(Fore.LIGHTMAGENTA_EX + "1. Simple Operations")
    print("2. Advanced Operations")
    print("3. Trigonometric Operations")
    print("")


    choice= input(Fore.LIGHTRED_EX + "Enter your choice: ")
    print("")

    if choice == "1":

        obj= simple_operations()
        
        print(Fore.CYAN + "Simple Operations:")
        print(Fore.LIGHTMAGENTA_EX + "1. Addition ")
        print("2. Subtraction ")
        print("3. Multiplication ")
        print("4. Division ")

        choice= input(Fore.LIGHTRED_EX + "Enter your choice: ")
        print("")

        if choice == "1":
            a = float(input("Enter the first number: "))
            b = float(input("Enter the second number: "))
            obj.add(a, b)
        
        elif choice == "2":
            a = float(input("Enter the first number: "))
            b = float(input("Enter the second number: "))
            obj.subtract(a, b)
        
        elif choice == "3":
            a = float(input("Enter the first number: "))
            b = float(input("Enter the second number: "))
            obj.multiply(a, b)
        
        elif choice == "4":
            a = float(input("Enter the first number: "))
            b = float(input("Enter the second number: "))
            obj.divide(a, b)
        
        else:
            print("Invalid choice")

    elif choice == "2":

        obj= advanced_operations()

        print("")
        print(Fore.CYAN + "Advanced Operations:")
        print(Fore.LIGHTMAGENTA_EX + "1. Square Root")
        print("2. Cube Root")
        print("3. Factorial")
        print("4. Power")
        print("5. Floor Value")
        print("")

        choice= input(Fore.LIGHTRED_EX + "Enter your choice: ")
        print("")

        if choice == "1":
            a = float(input("Enter the number: "))
            obj.square_root(a)
        
        elif choice == "2":
            a = float(input("Enter the number: "))
            obj.cube_root(a)
        
        elif choice == "3":
            a = int(input("Enter the number: "))
            obj.factorial(a)
        
        elif choice == "4":
            a = float(input("Enter the first number: "))
            b = float(input("Enter the second number: "))
            obj.power(a, b)

        elif choice == "5":
            a = float(input("Enter the first number: "))
            b = float(input("Enter the second number: "))
            obj.floor(a, b)
        
        else:
            print("Invalid choice")

    elif choice == "3":
        
            obj= trigonometric_operations()

            print("")
            print(Fore.CYAN + "Trigonometric Operations:")
            print(Fore.LIGHTMAGENTA_EX + "1. Sine  2. Cosine  3. Tangent")
            print("4. Hyperbolic Sine  5. Hyperbolic Cosine  6. Hyperbolic Tangent")
            print("")
        
            choice= input(Fore.LIGHTRED_EX + "Enter your choice: ")
            print("")
        
            if choice == "1":
                a = float(input("Enter the number: "))
                obj.sin(a)
            
            elif choice == "2":
                a = float(input("Enter the number: "))
                obj.cos(a)
            
            elif choice == "3":
                a = float(input("Enter the number: "))
                obj.tan(a)
            
            elif choice == "4":
                a = float(input("Enter the number: "))
                obj.sinh(a)
            
            elif choice == "5":
                a = float(input("Enter the number: "))
                obj.cosh(a)
            
            elif choice == "6":
                a = float(input("Enter the number: "))
                obj.tanh(a)
                
            else:
                print("Invalid choice")

    else:
        print("Invalid choice")

    print("")
    print(Fore.CYAN + "Would you like to perform another operation? (1 for Yes and 2 for No)")
    print("1.Yes  2.No")
    print("")
    choice= input("Enter your choice: ")

    if choice == "1":
        continue
    else:
        break

print("")
print(Fore.LIGHTGREEN_EX + Style.BRIGHT +"Thank you for using the Mini Calculator!")
print(Style.RESET_ALL)
print("")

