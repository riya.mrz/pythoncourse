#My Mini Calculator
#colour not added

class simple_operations:    #class for simple operations (addition, subtraction, multiplication, division)
    def add(self, a, b):
        result=a + b
        print("The sum of the two numbers is: ", result)

    def subtract(self, a, b):
        result= a - b
        print("The difference of the two numbers is: ", result)

    def multiply(self, a, b):
        result= a * b
        print("The product of the two numbers is: ", result)

    def divide(self, a, b):
        try:
            result = a / b
            print("The quotient of the two numbers is: ", result)
        except ZeroDivisionError: #exception handling for division by zero
            print("Error: Division by zero is not allowed.")

class advanced_operations: #class for advanced operations (square root, cube root, factorial, power, floor value)
    def square_root(self, a):
        result= a ** 0.5
        print("The square root of the number is: ", result)

    def power(self, a, b):
        result= a ** b
        print("The result of the number raised to the power is: ", result)
    
    def factorial(self, a):
        result= 1
        for i in range(0, a+1):
            result= result * i
        print("The factorial of the number is: ", result)

    def cube_root(self, a):
        result= a ** (1/3)
        print("The cube root of the number is: ", result)

    def floor(self, a,b):
        result= a // b
        print("The floor value of the number is: ", result)

import math #importing math module for trigonometric operations
class trigonometric_operations: #class for trigonometric operations (sine, cosine, tangent, hyperbolic sine, hyperbolic cosine, hyperbolic tangent)
    def sin(self, a):
        result= math.sin(a)
        print("The sine of the number is: ", result)

    def cos(self, a):
        result= math.cos(a)
        print("The cosine of the number is: ", result)

    def tan(self, a):
        result= math.tan(a)
        print("The tangent of the number is: ", result)

    def sinh(self, a):
        result= math.sinh(a)
        print("The hyperbolic sine of the number is: ", result)

    def cosh(self, a):
        result= math.cosh(a)
        print("The hyperbolic cosine of the number is: ", result)

    def tanh(self, a):
        result= math.tanh(a)
        print("The hyperbolic tangent of the number is: ", result)


print("...Mini Calculator...")
print("")

print("What kind of operation would you like to perform?")
print("1. Simple Operations  2. Advanced Operations  3. Trigonometric Operations")
print("")

choice= input("Enter your choice: ")

if choice == "1":

    obj= simple_operations()
    
    print("Simple Operations:")
    print("1. Addition  2. Subtraction  3. Multiplication  4. Division")

    choice= input("Enter your choice: ")

    if choice == "1":
        a = float(input("Enter the first number: "))
        b = float(input("Enter the second number: "))
        obj.add(a, b)
    
    elif choice == "2":
        a = float(input("Enter the first number: "))
        b = float(input("Enter the second number: "))
        obj.subtract(a, b)
    
    elif choice == "3":
        a = float(input("Enter the first number: "))
        b = float(input("Enter the second number: "))
        obj.multiply(a, b)
    
    elif choice == "4":
        a = float(input("Enter the first number: "))
        b = float(input("Enter the second number: "))
        obj.divide(a, b)
    
    else:
        print("Invalid choice")

elif choice == "2":

    obj= advanced_operations()

    print("Advanced Operations:")
    print("1. Square Root  2. Cube Root  3. Factorial  4. Power  5. Floor Value")

    choice= input("Enter your choice: ")

    if choice == "1":
        a = float(input("Enter the number: "))
        obj.square_root(a)
    
    elif choice == "2":
        a = float(input("Enter the number: "))
        obj.cube_root(a)
    
    elif choice == "3":
        a = int(input("Enter the number: "))
        obj.factorial(a)
    
    elif choice == "4":
        a = float(input("Enter the first number: "))
        b = float(input("Enter the second number: "))
        obj.power(a, b)

    elif choice == "5":
        a = float(input("Enter the first number: "))
        b = float(input("Enter the second number: "))
        obj.floor(a, b)
    
    else:
        print("Invalid choice")

elif choice == "3":
    
        obj= trigonometric_operations()
    
        print("Trigonometric Operations:")
        print("1. Sine  2. Cosine  3. Tangent")
        print("4. Hyperbolic Sine  5. Hyperbolic Cosine  6. Hyperbolic Tangent")
    
        choice= input("Enter your choice: ")
    
        if choice == "1":
            a = float(input("Enter the number: "))
            obj.sin(a)
        
        elif choice == "2":
            a = float(input("Enter the number: "))
            obj.cos(a)
        
        elif choice == "3":
            a = float(input("Enter the number: "))
            obj.tan(a)
        
        elif choice == "4":
            a = float(input("Enter the number: "))
            obj.sinh(a)
        
        elif choice == "5":
            a = float(input("Enter the number: "))
            obj.cosh(a)
        
        elif choice == "6":
            a = float(input("Enter the number: "))
            obj.tanh(a)
            
        else:
            print("Invalid choice")

else:
    print("Invalid choice")