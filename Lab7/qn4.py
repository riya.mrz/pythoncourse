# Write a program to swap two values using tuple assignment

myTuple = (1, 2)
print(myTuple)
a, b = myTuple
b,a=a,b
myTuple = (a, b)
print(myTuple)
