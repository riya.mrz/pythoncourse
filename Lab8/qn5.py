#wap that reads a text file and creates another file that is identical except that every sequence of consecutive blank spaces is replaced by a single space.

fname=input("Enter the filename:")

f=open(fname,"r")
content=f.read()
f.close()

fname=input("Enter the filename for the new file:")

f=open(fname,"w")   
f.write(content.replace("  "," "))
f.close()
