#     *
#    * *
#   * * *
#  * * * *
# * * * * *

def print_star(n):
    for i in range(1,n+1):
        print(" "*(n-i),end="")
        print("* "*i)

n=int(input("Enter the number of rows: "))
print_star(n)
